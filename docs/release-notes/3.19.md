Aegir 3.19.2
============

The Aegir team is proud to announce a new release in the stable 3.x branch!

This release ships with a number of bug fixes and UI improvements.



| Project   | Description | Links | Status |
|-----------|------------ |-------------------|--------|
| Provision | Drush commands | [drupal.org](https://www.drupal.org/project/provision) <br /> [github.com](https://github.com/aegir-project/provision) <br /> [gitlab.com](https://gitlab.com/aegir/provision)| [![build status](https://gitlab.com/aegir/provision/badges/7.x-3.x/build.svg)](https://gitlab.com/aegir/provision/)|
| Hosting | Drupal Modules| [drupal.org/project/hosting](https://www.drupal.org/project/hosting) <br /> [github.com/aegir-project/hosting](https://github.com/aegir-project/hosting) | |
| Hostmaster | Drupal Install Profile |[drupal.org/project/hostmaster](https://www.drupal.org/project/hostmaster) <br /> [github.com/aegir-project/hostmaster](https://github.com/aegir-project/hostmaster) | |
| Dockerfiles | Docker image definitions. Used for all tests. | [github.com/aegir-project/dockerfiles](https://github.com/aegir-project/dockerfiles)<br /> [hub.docker.com/r/aegir/hostmaster](https://hub.docker.com/r/aegir/hostmaster) | |
| Tests | Behat tests and Travis tools | [github.com/aegir-project/tests](https://github.com/aegir-project/tests) | |
| Development Environment | Local docker-based development environment. | [github.com/aegir-project/development](https://github.com/aegir-project/development) | [![Build Status](https://travis-ci.org/aegir-project/development.svg?branch=master)](https://travis-ci.org/aegir-project/development) |


Installing and upgrading
------------------------

The canonical source of installation documentation is

[http://docs.aegirproject.org/en/3.x/install/](http://docs.aegirproject.org/en/3.x/install/)

Within those sections you'll find step-by-step instructions for performing both [manual](/install/upgrade/#manual-upgrade) and [automatic](/install/upgrade/#upgrades-with-upgradesh-script) upgrade processes.

It is still imperative that you read the upgrade path and version-specific information and follow all version-specific upgrade instructions before trying to run the upgrade script or manual upgrade.


Need help?
----------

If you struggle to install or upgrade your Aegir system, you have a number of options available to you for getting help.

Consult this page for more information: [http://docs.aegirproject.org/en/3.x/community](http://docs.aegirproject.org/en/3.x/community)

Thanks to our awesome [community](http://docs.aegirproject.org/en/3.x/community) for their help, support and encouragement as always! Enjoy the new release :)


Changes
-------

### Major notables

* Drupal 9 related updates

### Complete list

**Changes to provision since 7.x-3.191**

* Issue #3186056: Undo the rest of the 7.x-3.x-devshop merge


**Changes to provision since 7.x-3.190**

* [#3186056](https://www.drupal.org/project/provision/issues/3186056): Error Verify Site
	* Reverted two commits that were merged too early via the 3.x-devshop branch

**Changes to hostmaster since 7.x-3.180**

* Update views and views_bulk_operations
* [#3153164](https://www.drupal.org/node/3153164) by [helmo](/u/helmo): Update dehydrated


**Changes to provision since 7.x-3.180**

* [#3160565](https://www.drupal.org/node/3160565) by [sseidel](/u/sseidel), [martin_q](/u/martin_q): MySQL database backups are not using UTF8MB4
* Add hook_provision_db_username_alter
* Add hook_provision_db_options_alter
* [#3153028](https://www.drupal.org/node/3153028): Debian package for core 7.72
* [#3149961](https://www.drupal.org/node/3149961): Improve handling files permissions and ownership
* [#3149961](https://www.drupal.org/node/3149961): Nginx templates update
* [#3149961](https://www.drupal.org/node/3149961): Templates cleanup
* [#3150004](https://www.drupal.org/node/3150004): Drupal 9 support - Stop using deprecated functions in D8
* [#3149961](https://www.drupal.org/node/3149961): Improve the If external request was HTTPS but internal request is HTTP D6
* [#3150004](https://www.drupal.org/node/3150004): Drupal 9 support - Cleanup
* [#3149998](https://www.drupal.org/node/3149998): Auto-detect Drupal 8 and 9 codebase structure (Apache)
* [#3150004](https://www.drupal.org/node/3150004): Drupal 9 support - Add D9 compatible inc files
* [#3150004](https://www.drupal.org/node/3150004): Drupal 9 support - Use separate templates for all Drupal core versions
* [#3149961](https://www.drupal.org/node/3149961): system_rebuild_module_data() is deprecated in D8 and removed in D9
* [#3149961](https://www.drupal.org/node/3149961): db_table_exists() is deprecated in D8 and removed in D9, while field_info_field_map() is D7-only
* [#3149961](https://www.drupal.org/node/3149961): Do not run chgrp on private/temp because it’s not included in backups anymore
* [#3149961](https://www.drupal.org/node/3149961): Run registry-rebuild in drush_provision_drupal_post_provision_deploy()
* [#3150004](https://www.drupal.org/node/3150004): Drupal 9 support - Add $settings['config_sync_directory'] in D8 settings template for D9 compatibility
* [#3149961](https://www.drupal.org/node/3149961): Add $settings['file_temp_path'] in D8 settings template
* [#3149961](https://www.drupal.org/node/3149961): Improve the If external request was HTTPS but internal request is HTTP
* [#3149961](https://www.drupal.org/node/3149961): Fix ownership early enough
* [#3149961](https://www.drupal.org/node/3149961): Improve packages and profiles handling
* [#3149998](https://www.drupal.org/node/3149998): Auto-detect Drupal 8 and 9 codebase structure
* [#3149961](https://www.drupal.org/node/3149961): Nginx - modernize server template
* [#3149961](https://www.drupal.org/node/3149961): Nginx - Use 10 seconds micro-cache by default
* [#3149961](https://www.drupal.org/node/3149961): Nginx - Leave tcp_nodelay, tcp_nopush and keepalive_requests for /etc/nginx/nginx.conf
* [#3149961](https://www.drupal.org/node/3149961): Nginx - Sync fastcgi_cache in subdirs with main config
* [#3149961](https://www.drupal.org/node/3149961): Nginx - Add extra rewrite for legacy imagecache paths
* [#3149961](https://www.drupal.org/node/3149961): Nginx - [Option] Deny public access to webform uploaded files
* [#3149961](https://www.drupal.org/node/3149961): Nginx - Do not log /civicrm* requests, but micro-cache them and protect from bots
* [#3149961](https://www.drupal.org/node/3149961): Nginx - CDN needs gzip_http_version 1.1
* [#3149961](https://www.drupal.org/node/3149961): Nginx - Support for static ads.txt file in the sites/domain/files directory
* [#3145927](https://www.drupal.org/node/3145927): Fix ci job name
* [#3145926](https://www.drupal.org/node/3145926): Deprecate Debian Jessie - remove CI test
* [#3145927](https://www.drupal.org/node/3145927): Start CI testing Ubuntu 20.04 LTS
* [#3143165](https://www.drupal.org/node/3143165): Debian package for core 7.71
* [#3066146](https://www.drupal.org/node/3066146) by [spiderman](/u/spiderman), [ergonlogic](/u/ergonlogic), [memtkmcc](/u/memtkmcc), [colan](/u/colan): Merge branch '3066146-verify-subdir-parent' into 7.x-3.x
* Update CI image, and move overtrue/phplint to require-dev.
* Updating process requirement so it can be included in other projects with symfony 4 and updating vendor code.
* Composer updates
* Require ~3 process so it can use 4 as well.
* [#3022207](https://www.drupal.org/node/3022207) by [helmo](/u/helmo): [meta] Aegir 3.18.4 release (core security)
* [#2542236](https://www.drupal.org/node/2542236) by [colan](/u/colan): Excluded directories that shouldn't be in site backups, and added alter hook.
* Better error messages for missing "platform" code.
* Do not attempt to destroy a database if we don't know the name of it.
* [#3066146](https://www.drupal.org/node/3066146) by [spiderman](/u/spiderman), [ergonlogic](/u/ergonlogic), [memtkmcc](/u/memtkmcc), [colan](/u/colan), [llamech](/u/llamech): Automatically spawn a Verify task for parent on subdir site installation.
* Improve error message for "Platform not found" state: Show the directory and mention all of the problems that could exist.
* Merge branch '2704291-check-hostmaster' into 7.x-3.x
* Merge branch 'db-error' of git.drupal.org:project/provision into 7.x-3.x
* Merge branch '7.x-3.x' into 2794915-grant-all-hosts
* Merge branch '2948114-hostmaster-detect-on-verify' of git.drupal.org:project/provision into 7.x-3.x
* Merge branch '7.x-3.x' into 2995091-hostmaster-dl-destination
* Merge branch '3040646-settings-https' of git.drupal.org:project/provision into 7.x-3.x
* Merge branch '3055463-more-includes' into 7.x-3.x
* Merge branch '2666158-dont-backup-missing-site' into 7.x-3.x
* Merge branch '2960237-sync-alter' of git.drupal.org:project/provision into 7.x-3.x
* Merge branch '3073299-hook-environment' into 7.x-3.x
* [#3086062](https://www.drupal.org/node/3086062) by [helmo](/u/helmo), [colan](/u/colan): [meta] Aegir 3.18.3 release
* [#3085544](https://www.drupal.org/node/3085544): Update Debian repo PGP key
* [#3086062](https://www.drupal.org/node/3086062) by [helmo](/u/helmo), [colan](/u/colan): [meta] Aegir 3.18.2 release
* [#3085544](https://www.drupal.org/node/3085544) by [colan](/u/colan): Created new mini-release to trigger Debian repo PGP key update.
* [#2960237](https://www.drupal.org/node/2960237) by [jon-pugh](/u/jon-pugh): Sites on remote servers fail on Platforms that use git or composer, with Drupal in a subfolder
* Merge branch '7.x-3.x' of git.drupal.org:project/provision into 2960237-sync-alter
* Merge branch '2836185-force-reinstall' of git.drupal.org:project/provision into 7.x-3.x
* [#3066538](https://www.drupal.org/node/3066538): Cleanup useless aegir.services.yml files.
* [#3066538](https://www.drupal.org/node/3066538): Fixes cookie subdomains missing first part of subdomain for non-subdirectory sites.
* Merge branch '7.x-3.x' into 2960237-sync-alter
* [#3073299](https://www.drupal.org/node/3073299): Add hook_provision_prepare_environment() and documentation.
* [#2836185](https://www.drupal.org/node/2836185) by [jon-pugh](/u/jon-pugh), [helmo](/u/helmo): Allow "force-reinstall" drush option for "provision-install" command. Fix problem when forcing install of sites that failed the first time.
* [#3035707](https://www.drupal.org/node/3035707) by [colan](/u/colan): Stopped setting the installation profile in settings.php.
* Check proper bootstrap_max
* Check proper bootstrap_max.
* [#2960237](https://www.drupal.org/node/2960237) by [jon-pugh](/u/jon-pugh): Sites on remote servers fail on Platforms that use git or composer, with Drupal in a subfolder. Allow other tools to alter the sunc path.
* [#2666158](https://www.drupal.org/node/2666158) by [jon-pugh](/u/jon-pugh), [helmo](/u/helmo), [ergonlogic](/u/ergonlogic): Don't invoke a "provision-backup" command if there is not site database available. Log warnings if this happens.
* Add missing golden contribs
* Revert "change version information for release 3.180"
* [#3055463](https://www.drupal.org/node/3055463): Add sites/all/settings.php to the include list.
* [#3040646](https://www.drupal.org/node/3040646): Detect HTTPS requests and set HTTPS variable
* [#2995091](https://www.drupal.org/node/2995091): Add "destination" to Hostmaster alias so "drush dl module" commands put the module in the right folder, also for updated installations.
* [#2995091](https://www.drupal.org/node/2995091) by [jon-pugh](/u/jon-pugh): Add "destination" to Hostmaster alias so "drush dl module" commands put the module in the right folder
* [#2995091](https://www.drupal.org/node/2995091): Add "destination" to Hostmaster alias so "drush dl module" commands put the module in the right folder
* [#2948114](https://www.drupal.org/node/2948114): In rare case, current hostmaster detection throws false positives when trying to write drushrc.php file
* Output command on error for generate_dump(), The error could be empty.
* [#2794915](https://www.drupal.org/node/2794915): Add 'db_grant_all_hosts' option to hostmaster-install command, so we can use it when automating aegir installation.
* [#2794915](https://www.drupal.org/node/2794915): Allow database servers to grant access to all hosts. Creates a property on database servers called "db_grant_all_hosts". If set to true, grant_host_list() will return %, allowing access from any host.
* [#2704291](https://www.drupal.org/node/2704291) by [jon-pugh](/u/jon-pugh): When checking if the site being verified is hostmaster, compare root and uri to the @hostmaster alias instead of just checking if a function exists.


**Changes to hosting since 7.x-3.180**

* [#3065477](https://www.drupal.org/node/3065477) by [millenniumtree](/u/millenniumtree): Task log is empty if severity levels had been configured
* [#3020169](https://www.drupal.org/node/3020169): Caught an error on this when installing in the d7 composer stack.
* [#3020169](https://www.drupal.org/node/3020169): Refactoring the fix to properly remove non-existant permissions before trying to add them.
* Improving hosting_get_new_tasks().
* Merge branch '3020169-permission-check' of git.drupal.org:project/hosting into 7.x-3.x
* [#3019462](https://www.drupal.org/node/3019462) by [jon-pugh](/u/jon-pugh): User with only "view" or "administer server" permissions and no client can not see or edit any servers
* [#3019462](https://www.drupal.org/node/3019462): User with only "view" or "administer server" permissions and no client can not see or edit any servers
* Fixing lingering missing functions.
* [#3020169](https://www.drupal.org/node/3020169): Installing hosting.module without admin_menu breaks because permissions are hard coded
* Change drush log types when updating profile metadata.
* Merge branch '2754069-site-profile-log' of git.drupal.org:project/hosting into 7.x-3.x
* Ensure $node->task_args always exists as an array.
* Reverting this change, did not realize it was processing all properties.
* Merge branch '2828630-hosting-queued-logs' into 7.x-3.x
* [#2944853](https://www.drupal.org/node/2944853): Simplify code by just casting to (int).
* Merge branch 'dev/fix-login-links' into 7.x-3.x
* Merge branch '2663710-retry' into 7.x-3.x
* [#2992202](https://www.drupal.org/node/2992202): Fix access check. I thought drupal_valid_path() also checked aliases?
* [#2663710](https://www.drupal.org/node/2663710): Show the retry button on any completed task. Change the name of the button to "Run Again" if it didn't end in error.
* [#2663710](https://www.drupal.org/node/2663710): Retrying a task should create a new task node, not a new revision
* Add Drush command to clear stale task queue locks.
* Fixes [#3034235](https://www.drupal.org/node/3034235): Use alias redirection target in login links.
* [#3018114](https://www.drupal.org/node/3018114): Allow users without a client to create a site if there is a default client and the setting is set
* [#2828630](https://www.drupal.org/node/2828630): Add a drush option to allow optionally not forking task processes.
* Merge branch '7.x-3.x' into 2828630-hosting-queued-logs
* [#2828630](https://www.drupal.org/node/2828630): Use "delta" not "duration". "duration" is from DevShop.
* [#2828630](https://www.drupal.org/node/2828630): Add $backend_options as the function argument so hosting-queued can use "interactive" and hosting-tasks can use "fork".
* [#2828630](https://www.drupal.org/node/2828630): Add duration to finished task message, and if task is executed in a forked process, change the message to "Launched task".
* Merge branch '7.x-3.x' of git.drupal.org:project/hosting into 2828630-hosting-queued-logs
* [#2828630](https://www.drupal.org/node/2828630): Add a new function to execute a hosting task: hosting_task_execute();  Used by drush hosting-tasks and drush hosting-queued commands.
* [#2828630](https://www.drupal.org/node/2828630): Improve hosting-tasks and hosting-queued logging by outputting the number of tasks found.
* [#2824731](https://www.drupal.org/node/2824731): Add site node to log message.
* [#2824731](https://www.drupal.org/node/2824731): Fix the query to lookup the profile_name, and output a log message.
* [#2824731](https://www.drupal.org/node/2824731): If site node 'profile' is empty and 'profile_name' is not, when a site task is loaded lookup the install profile package and save it to the site node.
* Merge branch '2824731-site-create-platform' into 2754069-decouple-install
* [#2828630](https://www.drupal.org/node/2828630) by [jon-pugh](/u/jon-pugh), [helmo](/u/helmo): move this logging to common functions
* [#2828630](https://www.drupal.org/node/2828630) by [jon-pugh](/u/jon-pugh), [helmo](/u/helmo): Make the log format more consistent ... trailing dot.
* Merge branch '2828630-hosting-queued-logs' of git.drupal.org:project/hosting into 2828630-hosting-queued-logs
* [#2828630](https://www.drupal.org/node/2828630): Fixing bad call to dt();
* Fixing bad call to dt();
* [#2828630](https://www.drupal.org/node/2828630): More verbose drush logs for drush hosting-tasks command. Shortening text.


**Changes to eldir since 7.x-3.180**

* [#3062389](https://www.drupal.org/node/3062389) by [josephleon](/u/josephleon), [colan](/u/colan): Fixed log message spillage.


**Changes to hosting_civicrm since 7.x-3.180**

* Move the Ansible integration to hosting_civicrm_ansible
* Separate the shortname generation into a new function so that we can call it from other modules
* Add support for CiviCRM/Drupal8 outside the web directory (and add support for Drupal9)
* Implements hook_ansible_inventory_alter
* Fix broken SQL in hosting_civicrm_get_sites()
* coopsymbiotic/ops[#142](https://www.drupal.org/node/142) Fix broken Mosaico templates after a clone/migrate.
* drush/install.provision.inc: add translation support for Armenian


**Changes to hosting_git since 7.x-3.180**

* Improving error messages when deciding to clone or not.
* Merge branch '3055846-web-ip-access' into 7.x-3.x
* Ensure repo path is not empty before trying to use it.
* [#2960237](https://www.drupal.org/node/2960237): Handle running on platform verify.
* [#2960237](https://www.drupal.org/node/2960237) by [jon-pugh](/u/jon-pugh): Sites on remote servers fail on Platforms that use git or composer, with Drupal in a subfolder
* Default github IP addresses have changed.
* [#3055846](https://www.drupal.org/node/3055846): Create a function to determine webhook access


**Changes to hosting_remote_import since 7.x-3.180**

* None


**Changes to hosting_site_backup_manager since 7.x-3.180**

* None


**Changes to hosting_tasks_extra since 7.x-3.180**

* [#3095961](https://www.drupal.org/node/3095961): Make sudo calls non-interactive.


**Changes to hosting_logs since 7.x-3.180**

* Use "hostmaster.error.log" for hostmaster logs instead of the URL.
* Fix incorrect loading of logs metadata.
* Data is now an array, load it that way.
* Load default logs_* properties into the node if there is no record in the database.


**Changes to hosting_https since 7.x-3.180**

* [#3153164](https://www.drupal.org/node/3153164): Update dehydrated
* [#3086056](https://www.drupal.org/node/3086056): Update bundled dehydrated to v0.6.5


**Changes to hosting_deploy since 7.x-3.2**

* [#2960237](https://www.drupal.org/node/2960237) by [drou7](/u/drou7), [ergonlogic](/u/ergonlogic): Adjust sync path when platform is built with Composer and Git.
* [#3065370](https://www.drupal.org/node/3065370) by [memtkmcc](/u/memtkmcc), [colan](/u/colan): Make the default option in hosting_deploy_platform_deploy_strategies() user friendly.
* [#3038678](https://www.drupal.org/node/3038678) by [colan](/u/colan): On Add Platform page, added link to documentation.
* [#3058033](https://www.drupal.org/node/3058033) by [colan](/u/colan): Mention that module can simply be enabled; it's already installed.


Known Issues
------------

* [#3186056](https://www.drupal.org/project/provision/issues/3186056): Error Verify Site
* If you've previously set the types of logs messages to display in task logs (under Administration » Hosting » Settings), your task logs may show up empty.  See [Task log is empty if severity levels had been configured](https://www.drupal.org/project/hosting/issues/3065477) for the workaround.
* The install test on Debian oldstable (jessie) kept failing, ignored for now.
* [Cookie subdomains missing first part of subdomain for non-subdirectory sites](https://www.drupal.org/project/provision/issues/3066538)
* Subdirectory site issues
    * [Migrating a subdirectory site reverts to internal domain & loses alias](https://www.drupal.org/project/provision/issues/3068257)
    * [hosting_subdirs fails to clean up subdir.d folder when sites are deleted](https://www.drupal.org/project/hosting/issues/3068595)
* If you separately installed the [aegir_rules](https://www.drupal.org/project/aegir_rules) integration module, be sure you have atleast the 3.0 version. Earlier beta's will cause an upgrade error.
* When the MYSQL password policy was set to MEDIUM creating databases can fail, see [issue 2868803](https://www.drupal.org/project/hostmaster/issues/2868803) for more info.
* When upgrading from Aegir 2.x be aware of [these notes](../install/upgrade/#major-upgrade-from-aegir-6x-2x).

For a full list of issues, see our combined [issue queues](https://www.drupal.org/project/issues?projects=provision%2C+hosting%2C+eldir%2C+Hostmaster+%28Aegir%29%2C+Aegir+Hosting+Git%2C+Aegir+Hosting+tasks+extra%2C+Aegir+Hosting+Logs%2C+Hosting+Site+Backup+Manager%2C+Aegir+Hosting+Remote+Import%2C+Aegir+Hosting+CiviCRM)
